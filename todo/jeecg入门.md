---
title: jeecg
tags: jeecg
categories: jeecg
mermaid: true
date: 2022-04-25
---





## 后台代码概要 
    jeecg-boot/jeecg-boot-module-system
```  
modules
├── api             // 开放接口
├── cas             // CAS单点登录
├── message         // 消息通知
├── monitor         // 监控模块 
├── oss             // 对象存储 阿里云
├── quartz          // 定时任务
├── system          // 管理模块 权限用户

```
 

## 后台功能介绍
``` 
├── api             // 开放接口
├── cas             // CAS单点登录
├── message         // 消息通知
├── monitor         // 监控模块
├── ngalain         // 
├── oss             // 对象存储 阿里云
├── quartz          // 定时任务
├── system          // 管理模块 权限用户
├──
```

## 功能记录

+ 系统管理 

| 模块     | 描述        |
| ------- | ---------- | 
| 租户管理 | 区别于部门,无级联<br/>jwtfilter读取请求中的tenant-id区别租户<br/>区别租户的表在org.jeecg.config.mybatis.MybatisPlusSaasConfig补充    |
| 部门管理 | 标准部门管理,支持跨部门,级联                |
| 用户管理 | 管理后台的用户管理,多角色/多租户/单/工作流<br>登录前选择租户,登录后切换部门    |

+ 系统权限用法 
    http://doc.jeecg.com/2044036
1. 数据权限规则:新增之后,在角色管理中配置规则中启动,即可在此模块对应的SQL中追加and条件
2. 页面元素根据配置的"授权标识"进行判断是否显示(v-has). 用户管理这个功能没配置vue文件 

+ Online开发
    http://doc.jeecg.com/2044056 
    无代码可视化 不利于后期维护

 
   
## 场景使用
+ 一般代码生成(多表)
+ 无表的页面生成

