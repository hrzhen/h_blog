---
title: k8s
tags: [k8s,云原生]
categories: 云原生
mermaid: true
date: 2021-12-03
---   

#### 功能
+ 服务发现,即微服务中的注册中心
+ 容器编排:管理多个容器的生命周期
+ 存储编排:自动挂载不同存储系统,本地目录/云对象存储...
+ 自动部署跟回滚: 根据自定义规则,进行不同版本的容器启动
+ 资源自伸缩: 限制容器对资源的使用(CPU内存),并可动态增加节点扩容性能
+ 自动修复: k8s集群/容器等自动重启
+ 密钥与配置管理:即微服务的配置中心



#### 工作方式
> n主节点+m工作节点,其中n,m>1
+ 主节点: 调度工作节点服务
    + kube-controller-manager: k8s中所有资源对象的自动化控制中心，维护管理集群的状态，比如故障检测，自动扩展，滚动更新等,包括节点/副本中的pod/端点/服务许可,token
    + kube-scheduler: 负责资源调度，按照预定的调度策略将Pod调度到相应的机器上
    + etcd: 分布式存储介质
    + api server: 暴露Kubernetes API
    + cloud-controller-manager: 与底层云提供商的平台交互,包括节点/路由/服务/卷
+ 工作节点: 实际部署跟使用容器
    + kubelet：负责Pod对应的容器的创建、启停等任务，同时与Master密切协作，实现集群管理的基本功能
    + kube-proxy：通过在主机上维护网络规则并执行连接转发来实现Kubernetes服务抽象
    + Docker/rkt：容器引擎，负责本机的容器创建和管理


## 概念
+ Namespace 名称空间,隔离资源
+ 对象,即yml文件
```
apiVersion: apps/v1beta1 # 创建对象的Kubernetes API 版本
kind: Deployment # 对象类型
metadata: #  具有唯一标示对象的数据，包括 name（字符串）、UID和Namespace（可选项）
  name: nginx-deployment
spec:
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```
+ Pod k8s的最小单元,多个容器组合在一起得共享资源(存储/网络/容器版本)
+ service 将一组pod 公开为网络服务的抽象方法
+ PV：持久卷（Persistent Volume），将应用需要持久化的数据保存到指定位置
+ PVC：持久卷申明（Persistent Volume Claim），申明需要使用的持久卷规格
+ ConfigMap 抽取应用配置，并且可以自动更新(中间件支持热更新的话 配置就是热更新的). 
+ Secret 对象类型用来保存敏感信息


无状态应用 deployment
    
    可取代,无
    比如:  nginx/微服务
有状态副本集 

    mysql/redis/zookeeper  

## K3S

```
https://docs.rancher.cn/docs/k3s/quick-start/_index/
https://kuboard.cn/install/v3/install.htm
https://juejin.cn/post/6952331691524358174#heading-10



查看pod日志
kubectl logs -f --namespace=kube-system octopus-ui-77f99f8697-gfr7p

查看所有pod
kubectl get pods --all-namespaces

删除pod
kubectl delete pod octopus-ui-77f99f8697-gfr7p -n kube-system

安装k3s
https://docs.rancher.cn/docs/k3s/quick-start/_index/ 

#install k3s
$  curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn INSTALL_K3S_EXEC="--docker --no-deploy traefik --flannel-backend=none"  sh -


#check node
$ k3s kubectl get node

#install dashboard
$ kubectl apply -f https://addons.kuboard.cn/kuboard/kuboard-v3.yaml
``` 


### 参考连接
+ [kubesphere官方文档](https://v2-1.docs.kubesphere.io/docs/zh-CN/release/release-v211/)
