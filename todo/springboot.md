---
title: springboot
tags: [springboot]
categories: springboot
mermaid: true
date: 2023-03-20
---  

## 问题记录

####  异步调用失效

1. 在需要用到的 @Async 注解的类上加上 @EnableAsync，或者直接加在springboot启动类上；
2. 异步处理方法（也就是加了 @Async 注解的方法）只能返回的是 void 或者 Future 类型；
3. 同一个类中调用异步方法需要先获取代理类，因为 @Async 注解是基于Spring AOP （面向切面编程）的，而AOP的实现是基于动态代理模式实现的。有可能因为调用方法的是对象本身而不是代理对象，因为没有经过Spring容器 

####  单例失效
+ 配置文件(注解@ConfigurationProperties)的bean应该直接调用,而不是通过配置文件读取



