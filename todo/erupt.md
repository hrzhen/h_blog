

## 动态页面的接口备注
+ 获取视图的接口
https://www.erupt.xyz/demo/erupt-api/data/table/Goods
+ 参数,其中 linkTreeVal 查询的上级编码
``` 
 {"pageIndex":1,"pageSize":10,"condition":[{"key":"status","value":null}],"linkTreeVal":"7","sort":""}
```
+ 返回值
``` 
 {"eruptModel":{"eruptName":"Goods","eruptJson":{"layout":{"formSize":"DEFAULT","tableLeftFixed":0,"tableRightFixed":0},"primaryKeyCol":"id","rowOperation":[],"drills":[],"linkTree":{"dependNode":false,"field":"category"},"desc":"","tree":{"id":"id","label":"name","expandLevel":999,"pid":""}},"eruptFieldModels":[{"fieldName":"image","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"","title":"商品图片","viewType":"IMAGE","className":"","desc":""}],"edit":{"notNull":true,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"attachmentType":{"fileTypes":[],"fileSeparator":"|","maxLimit":6,"type":"IMAGE","size":-1,"path":""},"title":"商品图片","type":"ATTACHMENT","desc":"","search":{"notNull":false,"vague":false,"value":false}}},"value":null,"componentValue":null},{"fieldName":"name","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"","title":"商品名称","viewType":"TEXT","className":"","desc":""}],"edit":{"inputType":{"fullSpan":true,"type":"text","length":255,"prefix":[],"suffix":[]},"notNull":true,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"商品名称","type":"INPUT","desc":"","search":{"notNull":false,"vague":true,"value":true}}},"value":null,"componentValue":null},{"fieldName":"category","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"name","title":"所属分类","viewType":"TEXT","className":"","desc":""}],"edit":{"notNull":true,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"所属分类","referenceTreeType":{"id":"id","label":"name","expandLevel":999,"dependField":"","pid":"parent.id"},"type":"REFERENCE_TREE","desc":"","search":{"notNull":false,"vague":false,"value":true}}},"value":null,"componentValue":null},{"fieldName":"freight","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"","title":"运费","viewType":"TEXT","className":"","desc":""}],"edit":{"numberType":{"min":-2147483647,"max":2147483647},"notNull":true,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"运费","type":"NUMBER","desc":"","search":{"notNull":false,"vague":true,"value":true}}},"value":null,"componentValue":null},{"fieldName":"status","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"","title":"商品状态","viewType":"BOOLEAN","className":"","desc":""}],"edit":{"notNull":true,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"商品状态","boolType":{"trueText":"上架","falseText":"下架"},"type":"BOOLEAN","desc":"","search":{"notNull":false,"vague":false,"value":true}}},"value":null,"componentValue":null},{"fieldName":"description","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"","title":"商品描述","viewType":"HTML","className":"","desc":""}],"edit":{"notNull":false,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"商品描述","htmlEditorType":{"value":"UEDITOR","path":""},"type":"HTML_EDITOR","desc":"","search":{"notNull":false,"vague":false,"value":false}}},"value":null,"componentValue":null},{"fieldName":"goodsSpecs","eruptFieldJson":{"views":[],"edit":{"notNull":false,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"商品型号","type":"TAB_TABLE_ADD","desc":"","search":{"notNull":false,"vague":false,"value":false}}},"value":null,"componentValue":null},{"fieldName":"id","eruptFieldJson":{"views":[],"edit":{"numberType":{"min":-2147483647,"max":2147483647},"notNull":false,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"","type":"NUMBER","desc":"","search":{"notNull":false,"vague":false,"value":false}}},"value":null,"componentValue":null}],"searchCondition":{},"extraRow":false},"tabErupts":{"goodsSpecs":{"eruptModel":{"eruptName":"GoodsSpec","eruptJson":{"layout":{"formSize":"DEFAULT","tableLeftFixed":0,"tableRightFixed":0},"primaryKeyCol":"id","rowOperation":[],"drills":[],"desc":"","tree":{"id":"id","label":"name","expandLevel":999,"pid":""}},"eruptFieldModels":[{"fieldName":"name","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"","title":"名称","viewType":"TEXT","className":"","desc":""}],"edit":{"inputType":{"fullSpan":false,"type":"text","length":255,"prefix":[],"suffix":[]},"notNull":true,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"名称","type":"INPUT","desc":"","search":{"notNull":false,"vague":false,"value":false}}},"value":null,"componentValue":null},{"fieldName":"price","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"","title":"价格","viewType":"TEXT","className":"","desc":""}],"edit":{"numberType":{"min":-2147483647,"max":2147483647},"notNull":true,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"价格","type":"NUMBER","desc":"","search":{"notNull":false,"vague":false,"value":false}}},"value":null,"componentValue":null},{"fieldName":"inventory","eruptFieldJson":{"views":[{"width":"","template":"","show":true,"tpl":{"width":"","enable":false},"sortable":false,"column":"","title":"库存","viewType":"TEXT","className":"","desc":""}],"edit":{"numberType":{"min":-2147483647,"max":2147483647},"notNull":true,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"库存","type":"NUMBER","desc":"","search":{"notNull":false,"vague":false,"value":false}}},"value":null,"componentValue":null},{"fieldName":"id","eruptFieldJson":{"views":[],"edit":{"numberType":{"min":-2147483647,"max":2147483647},"notNull":false,"placeHolder":"","show":true,"readOnly":{"edit":false,"add":false},"title":"","type":"NUMBER","desc":"","search":{"notNull":false,"vague":false,"value":false}}},"value":null,"componentValue":null}],"searchCondition":{},"extraRow":false},"tabErupts":null,"combineErupts":null,"operationErupts":null,"power":{"add":true,"delete":true,"edit":true,"query":true,"viewDetails":true,"export":false,"importable":false}}},"combineErupts":null,"operationErupts":null,"power":{"add":true,"delete":true,"edit":true,"query":true,"viewDetails":true,"export":false,"importable":false}}
```



## 自定义数据源
1. 实现 IEruptDataService 接口. 主要是分页跟条件查询的处理 参考MongoDB模块
2. 实体类 增加 EruptDataProcessor 注解 





## erupt-cloud
1. 启动 server 去微节点增加节点配置,成功之后获取 Access Token
2. 配置需要启动的微节点配置
```yaml
erupt:
  cloud-node:
    server-addresses: ['http://127.0.0.1:11801']   # 必填: erupt-cloud-server 端部署地址
    access-token: P6IE4X5DREOT34CH # 必填: 从 server 端获取 access-token
    node-name: asset # 必填: 应用名从 server 端获取，要与server端配置的一致
    enable-register: true  # 可选：是否开启服务注册，默认值 true
    heartbeat-time: 15000  # 可选：心跳时间（毫秒），默认值 15000
```
3. 配置菜单
> 比如配置名格式为 ${node-name}.${modelClassName}  比如 asset.User
