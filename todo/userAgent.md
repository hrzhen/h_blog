---
title: 
tags: [浏览器]
categories: web
mermaid: true
date: 2022-03-23
---  

### 说明
+ userAgent
> 浏览器的用户代理.
> 在粗略判断打开页面的浏览器/客户端时可以使用
> 比如

```   
        let thatClient = '其他浏览器'
        if (/MicroMessenger/.test(window.navigator.userAgent)) {
           thatClient = '微信客户端';
        } else if (/AlipayClient/.test(window.navigator.userAgent)) {
            thatClient = '支付宝客户端';
        } else {
             
        }     
```

+ webdriver
> 正规爬虫的标记


+ 
>