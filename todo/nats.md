




## linux 单机运行
+ nats-server.conf
```
port: 4222
monitor_port: 8222

server_name: nats_t

jetstream {
  store_dir: "/www/data/nats/js-op-test"
  max_memory_store: 1073741824
  max_file_store: 10737418240
}


debug:   true
trace:   true
logtime: true
logfile_size_limit: 1GB
log_file: "/www/data/nats/nats-server.log"
```

+ 执行
```
## 能运行但JetStream不能正常使用
./nats-server -js -c=nats-server.conf 
```

## docker 

+ 执行. 8222不能访问,不可明显看到队列的存储结果(是否消费),没持久化
``` 
docker run --restart=always -d -p 4222:4222 -p 8222:8222 -p 6222:6222 --name nats -ti nats:latest  -js 
```