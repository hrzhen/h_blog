---
title: window-c
tags: [window]
categories: 工具
mermaid: true
date: 2022-07-29
---  

###  msys2配置gcc环境
1. 下载安装
2. 进入msys2安装目录 增加源(第一行)

``` 
-- 文件 /etc/pacman.d/mirrorlist.mingw32
Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/i686

-- 文件 /etc/pacman.d/mirrorlist.mingw64 
Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/x86_64

-- 文件 /etc/pacman.d/mirrorlist.msys
Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/msys/$arch

```
3. 点击安装目录的 msys2.exe   
```
-- 更新源和msys2
pacman -Syu

-- 下载 MinGW-w64 编译器
pacman -S mingw-w64-x86_64-toolchain
```
4. msys2 安装目录下的 ~/.bashrc 增加环境变量
``` 
export PATH=$PATH:/mingw64/bin/

```
5. 配置系统的环境变量 path追加 [msys2安装目录]\mingw64\bin

6. 下载 xmake  


-- TODO 

7. 创建xmake项目

8. 运行测试

9. 打包测试

### 参考
+ [msys2](https://www.msys2.org/)
+ [清华-msys2下载](https://mirrors.tuna.tsinghua.edu.cn/msys2/distrib/)
