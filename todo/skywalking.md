---
title: skywalking
tags: [低代码开发]
categories: web
mermaid: true
date: 2022-03-25
---  

## 安装(docker-compose)

+ 18080 ui开放的端口 
+ 11800 探针对接的端口
+ 12800 ui获取探针聚合的端口

``` 
version: '3'
services:
  skywaling-oap:
    image: apache/skywalking-oap-server:9.3.0
    container_name: oap
    restart: always
    ports:
      - 11800:11800
      - 12800:12800
  skywaling-ui:
    image: apache/skywalking-ui:9.3.0
    container_name: ui
    depends_on:
      - oap
    links:
      - oap
    ports:
      - 18080:8080
    environment:
      - SW_OAP_ADDRESS=http://oap:12800
```


## 使用
### Java
    探针默认读取其配置文件的参数,可通过启动脚本覆盖默认配置,参考如下
+ 下载探针 
+ 配置服务启动命令
```
-javaagent:D:\\soft\\skywalking-agent\\skywalking-agent.jar
-DSW_AGENT_COLLECTOR_BACKEND_SERVICES=10.0.0.25:11800
-DSW_AGENT_NAME=qtp
```

+ 测试结果


### JavaScript

+ 安装探针
```
npm install skywalking-client-js --save  --registry=https://registry.npm.taobao.org
```

+ 配置(vue,webpack)
```
import ClientMonitor from 'skywalking-client-js';

Vue.config.errorHandler = (error) => {
  ClientMonitor.reportFrameErrors({
    collector: 'http://127.0.0.1',
    service: 'vue-demo',
    pagePath: '/app',
    serviceVersion: 'v1.0.0',
  }, error);
}
```
+ 测试
 


