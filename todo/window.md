---
title: bat脚本
tags: []
categories: 
mermaid: true
date: 2021-07-27
---  

### 使用
1. 遍历文件
````
@echo off
:: 遍历当前文件夹下的所有后缀名为pdf的文件
for /r %%i in (*.pdf) do ( echo %%i )
pause

````

2. 定时关机 (秒数)
```
shutdown -s -t 3600
```


3. 删除知道目录以及其文件
``` 
@ECHO OFF  
PAUSE 
:: %FOLDER% 当前目录
RD /Q/S "%FOLDER%1" 
RD /Q/S "%FOLDER%2" 
RD /Q/S "%FOLDER%3" 
EXIT
```

4. 7z解压压缩
``` 
:: 解压文件到当前目录
7z.exe x demo.zip
:: 压缩文件
7z e archive.zip -oc:\soft *.cpp -r
```

5. 屏蔽键盘
``` 
AutoHotkey 
qt
```