---
title: Apache ShenYu
tags:  网关
categories: 网关
mermaid: true
date: 2021-12-22
---   

## 使用
### 概述
> 基于netty,多插件热插拔.
> 相对 springboot gateway 来说二开成本较低.
> shenyu-bootstrap 参考使用的网关例子 .
> shenyu-admin 为管理端以及页面,修改相关配置同步到网关.


### 管理端使用
    服务转发举例,通过host转发到某个服务
1. 开启插件: [基础配置]-[插件管理]
2. 配置规则:关键配置
    + [插件列表]-[divide] 
    + 新增选择器:host=demo.top
    + 配置后台节点,ip:port为127.0.0.1:8080,注意权重跟顺序
    + 添加规则,条件 uri match /** 
    + 同步自定义divide
    
3. 若前置 nginx 转发
    + nginx需要配置请求头 ````` proxy_set_header X-Forwarded-For $host; `````
    + 网关实例化类覆盖原host获取 

``` 
@Bean
public RemoteAddressResolver customRemoteAddressResolver() {
    return new ForwardedRemoteAddressResolver(1);
}  
```

### 部分源码记录
+ 插件加载判断
```  
org.apache.shenyu.plugin.base.AbstractShenyuPlugin.execute
```

+ 插件加载顺序枚举类
``` 
org.apache.shenyu.common.enums.PluginEnum
```

+ 选择器/规则判断
```
org.apache.shenyu.plugin.base.condition
```

## 参考链接 
+ [Apache shenyu](https://shenyu.apache.org/zh/docs/index/ ) 


## 不正规使用
+ 配置同nginx的反向代理

+ 黑白名单

+ 动态修改请求参数

+ 自定义日志



---------------

### 常用插件

10 | global |  
50 | waf | 对流量实现防火墙
60 | rate_limiter | 基于redis的限流 
70 | param_mapping | 参数映射
80 | context_path | 重写请求路径的 contextPath
90 | rewrite | 对 uri 的重新定义 
110 | redirect | 重定向请求
120 | request |  请求参数、请求头以及 Cookie 来添加、修改、移除。
160 | logging | 控制台打印本次请求信息，包含请求路径、请求方法、请求参数和响应头、响应体等信息
170 | monitor | 监控自身运行状态（JVM相关），请求的响应迟延，QPS、TPS等相关metrics,通过prometheus跟Grafana进行可视化
200 | divide | 对http正向代理的负载均衡调用 
200 | websocket |  websocket协议支持
220 | modifyResponse | 对响应(状态码/请求头/请求体)进行修改
300 | paramTransform | 请求参数进行修改  
420 | response |   

 

### 重点
+ 重定向跟转发的区别跟场景
