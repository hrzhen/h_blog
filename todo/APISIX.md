



### 了解
+ 名词


| 名词   | 说明                                                        |
|---------------|-----------------------------------------------------------|
| 路由 routes     | 请求的入口点，它定义了客户端请求与服务之间的匹配规则                                |
| 上游 upstream   | 已创建的上游服务（即后端服务），可以对上游服务的多个目标节点进行负载均衡和健康检查                 |
| 服务 service    | 由路由中公共的插件配置、上游目标信息组合而成。服务与路由、上游关联，一个服务可对应一组上游节点、可被多条路由绑定。 |
|  消费者 consumer | 路由的消费方，形式包括开发者、最终用户、API 调用等。创建消费者时，需绑定至少一个认证类插件。          |

### 入门
+ docker-compose一建部署
``` 
-- 下载
git clone https://github.com/apache/apisix-docker.git


-- 授权执行
chmod -R 777 /opt/apisix-docker/example/
cd apisix-docker/example

docker-compose -p docker-apisix up -d 
```

+ 服务说明
```
web控制台 http://localhost:9000 

```
| 宿主机端口 | 说明  | 其他 |
| --- | ---- | ------- |     
9000 | web控制台 | 账号密码 admin admin
2379 | etcd | 
9090 | prometheus | 基于redis的限流 
3000 / grafana /
9080 /   /
9081/9082 / 测试的服务  /


### 问题
+ 网关必须有一个认证插件
+ 基于 


### 参考链接
+ [b站视频](https://www.bilibili.com/video/BV1Hy4y1172k?spm_id_from=333.999.0.0) 
+ [官网-快速入门](https://apisix.apache.org/zh/docs/apisix/getting-started/)


