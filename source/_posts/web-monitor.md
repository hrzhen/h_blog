---
title: 前端采集监控
tags: []
categories: 
mermaid: true
date: 2021-06-20
---  

### 需求
1. 解决用户使用的问题,错误记录
2. 提升用户使用,性能记录 操作记录
3. 注意上报的频率跟方式(不能影响用户,也不能把服务器搞垮)
4. 上报包括游客跟系统用户


### 参考
+ typeScript
>   https://github.com/mitojs/mitojs
>   https://github.com/clouDr-f2e/monitor
>   
+ 许久未更新
>   https://github.com/adminV/Vue-dataAc
>   https://github.com/wangweianger/web-report-sdk
