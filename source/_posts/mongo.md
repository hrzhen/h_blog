---
title: mongo
tags: [nosql]
categories: 数据库
mermaid: true
date: 2021-06-26
---  

### 入门
##### 简介
```
Nosql
基于分布式文件存储的开源数据库系统
不需要建表建库,自动判断
不限制数据结构,前后插入的结构可有所差别


```

##### 说明

SQL概念 |	MongoDB概念 |	说明 
-------------------- | --------------- | -----
database |	database |	数据库
table |	collection |	数据库表/集合
row |	document |	数据记录行/文档
column |	field |	数据字段/域
index |	index |	索引
table | joins |	 	表连接,MongoDB不支持
primary key |	primary key |	主键,MongoDB自动将_id字段设置为主键

##### 基础操作
+ 创建数据库
``` 
use [DATABASE_NAME] 
```

+ 查看所有数据库
``` 
show dbs
```

+ 插入记录 save/insert
```   
db.[DATABASE_NAME].insert({"uid":111,"desc":"欧克欧克"})  
``` 

+ 更新记录
```      
db.collection.update(    
	<query>, // {'age':17}
	<update>, // {$set:{'desc':'未成年'}}
	{       
		upsert: <boolean>,   // true
		multi: <boolean>,  // true
		writeConcern: <document>// 
	}
)
ps:
    query : update的查询条件，类似sql update查询内where后面的。
    update : update的对象和一些更新的操作符（如$,$inc...）等，也可以理解为sql update查询内set后面的
    upsert : 可选，这个参数的意思是，如果不存在update的记录，是否插入objNew,true为插入，默认是false，不插入。
    multi : 可选，mongodb 默认是false,只更新找到的第一条记录，如果这个参数为true,就把按条件查出来多条记录全部更新。
    writeConcern :可选，抛出异常的级别。
```  


+ 删除记录
```
db.collection.remove(     
	<query>,     // {'title':'旧文章'}
	{       
		justOne: <boolean>,
		writeConcern: <document> 
	} 
)
ps:
    query :（可选）删除的文档的条件。
    justOne : （可选）如果设为 true 或 1，则只删除一个文档。
    writeConcern :（可选）抛出异常的级别。

```


+ 其他函数参考官网


### 参考

+ [官网](https://www.mongodb.com/)