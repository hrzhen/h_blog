---
title: nginx-基本
tags: nginx
categories: nginx
mermaid: true
date: 2021-06-20
---


### 1 简介

+ 场景 http服务器 静态服务器 反向代理 负载均衡 动态代理 重写url
+ 文档 http://nginx.org/en/docs/
+ 其他版本 OpenResty, kong, Tengine, Apache APISIX

### 2 常用命令

+ 检查配置文件 nginx -t
+ 查看已安装的依赖 nginx -V
+ 启动 start nginx
+ 关闭 nginx -s stop
+ 重启 nginx -s reload

### 3 参考使用
+ 3.1 access_log 按日期存储
```  
    http {
        log_format  main  '$remote_addr - $remote_user [$time_iso8601] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';
        server {
            if ($time_iso8601 ~ '(\d{4}-\d{2}-\d{2})') {
                    set $current_day $1;
            }
            access_log  logs/access-$current_day.log  main;
            listen 80; 
        }
    }
```


+ 3.2 web集群: 这里需要注意几个时间设置
``` 
    http {
        include       mime.types; 
        keepalive_timeout  5; 
        upstream tomcat_server{
            server 127.0.0.1:8180 weight=90;
            server 127.0.0.1:8280 weight=10;
            server 127.0.0.1:8280 weight=10 backup;
        }
        server {
            listen       90;
            server_name  localhost;
            location / {
                root   html;
                index  index.html index.htm;
                proxy_pass http://tomcat_server;
            
                proxy_connect_timeout  2s; 
                proxy_send_timeout 2s;
                proxy_read_timeout 2s;            
                
                proxy_set_header Host $host; 
                proxy_set_header X-Forwarded-Host $server_name;
                proxy_set_header X-Real-IP $remote_addr; 
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            }        
        }
    }
 ```

+ 3.3 前后端分离

```  
        location / {
            alias /tn/tn/admin/dist;
            index index.html;
            try_files $uri $uri/ /index.html;
        } 
        
 	    location /java/ {
            proxy_set_header  Host              $http_host;
            proxy_set_header  X-Real-IP         $remote_addr;
            proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
            proxy_set_header  X-Forwarded-Proto $scheme;
            proxy_pass   http://127.0.0.1:8001;
        }
```  

+ 3.4 https 
```  
    http {
        server {
            ## https
            listen       443 ssl;
            listen       80;
            server_name  www.code.com;
            ssl_certificate      cert/x509.crt;
            ssl_certificate_key  cert/pri.key;
            ssl_protocols  SSLv2 SSLv3 TLSv1;
            ssl_ciphers  HIGH:!aNULL:!MD5;
            ssl_prefer_server_ciphers   on;
            ssl_session_timeout 5m;
            
            location / {
                root   html;
                index  index.html index.htm index.php;
                proxy_pass http://localhost:8081/; 
                proxy_set_header X-Forwarded-Proto https; # 转发时使用https协议 
                proxy_set_header REMOTE_ADDR $remote_addr; 
                proxy_set_header Host $http_host; 
                proxy_http_version 1.1; 
                proxy_set_header Connection ""; 
                proxy_set_header Upgrade $http_upgrade; 
                proxy_set_header Connection "upgrade";
            }
        }
    }
```  

+ 3.5  解决前端css等样式无法生效的bug.
```
    http{
        include       mime.types;
        default_type  application/octet-stream;   
    }
``` 

+ 3.6 根据请求地址分流前端.
```
     location / {
        if ( $remote_addr ~* ^(.*)\.(.*)\.(.*)\.[1,125]$) {
            root   html_dev;
            beark;
        }
        ## 若关闭上面路由 则需要通过404转发 error_page 404 = http://www.beautysaas.com/$1; 
        
        root   html;
        index  index.html index.htm;
     } 
            
```


+ 3.7 其他转发 
```  
# mysql
stream { 
    upstream mysql{
        hash $remote_addr consistent; 
        # $binary_remote_addr; 
        server 127.0.0.1:3306 weight=5 max_fails=3 fail_timeout=30s; 
    }

    server { 
        listen 13306;
        proxy_connect_timeout 10s; 
        proxy_timeout 30s;
        proxy_pass mysql; 
    } 
} 

# redis 
stream { 
    upstream redis{ 
        server 127.0.0.1:6379 ; 
    }

    server { 
        listen 16379;
        proxy_connect_timeout 10s; 
        proxy_timeout 3s;
        proxy_pass redis; 
    } 
}

# minio 
http {
    server { 
        proxy_set_header Host $host;## minio在校验signature是否有效的时候,必须从http header里面获取host
        proxy_pass http://127.0.0.1:9000;
    } 
}


# grafana 
https://grafana.com/tutorials/run-grafana-behind-a-proxy/
```

+ 3.8 其他
``` 
        ##  按顺序检查文件是否存在，返回第一个找到的文件,找不到就404
        try_files $uri $uri/ =404;

        ## 协商缓存
        add_header Cache-Control no-chace;##   http1.1
        add_header Pragma no-chace; ##   http1.1 跟 http1.0
```

### 4  要点
+ 负载均衡
> 每个请求按时间顺序逐一分配到不同的后端服务器，如果超过了最大失败次数后（max_fails,默认1），在失效时间内(fail_timeout，默认10秒)，该节点失效权重变为0。
> 超过失效时间后，则恢复正常，或者全部节点都为down后，那么将所有节点都恢复为有效继续探测，一般来说rr可以根据权重来进行均匀分配。
> 在节点被淘汰的时候,$upstream_addr的值是名字,而不是 地址加端口


### 5 追加插件
    以插件 http_auth_request_module 为例
1. nginx -V 获取已经安装的插件
2. find -name configure 查找原编译脚本目录
3. 在原执行脚本追加插件 如 ./configure --add-module.....[-V的arg参数]  --with-http_auth_request_module
4. 编译 make,  这里./objs/nginx 就是结果可执行文件
5. 补充相关依赖,复制/替换旧的nginx启动文件(通过),重启即可
6. nginx可执行文件不包括其依赖,所以在其他环境上不建议复制,而是重新增加插件

### 7 参考链接
+ [官方说明](http://nginx.org/en/docs/http/ngx_http_upstream_module.html#keepalive_requests)
+ [upstream针对后端服务器容错的配置说明](https://www.cnblogs.com/kevingrace/p/8185218.html)