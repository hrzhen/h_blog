
---
title: jib
tags: [docker,maven]
categories: 工具
date: 2021-06-21
--- 

### 问题
```
在项目多次迭代之后,打包更新的频率更显复杂
单纯通过远程工具到linux服务器操作,适合不经常更新的私有化部署
通过Jenkins加上shell脚本,可简化自动化流程
但进行多个项目拆分的时候,不熟悉运维的就得背锅了
同时为了后续容器化自动化运维做铺垫,整理相关文档记录
```


### 调研

+ 可用方式
1. Dockerfile 构建(最通用版)
2. maven插件 dockerfile-maven-plugin
3. maven插件 jib-maven-plugin

+ 分析
    + 方式1/2都需要编写 Dockerfile 
    + 为减少学习成本,以及装下,用方式3测试 

### 使用举例：
+ 修改pom.xml
````
 <plugin>
                <groupId>com.google.cloud.tools</groupId>
                <artifactId>jib-maven-plugin</artifactId>
                <version>1.6.0</version>
                <configuration>
                    <allowInsecureRegistries>true</allowInsecureRegistries>
                    <from>
                        <image>openjdk:8-jdk-alpine</image>
                    </from>
                    <to>
                        <image>simple-server</image>
                    </to>
                    <container>
                        <mainClass>xyz.herz.DemoApp</mainClass>
                        <jvmFlags>
                            <jvmFlag>-Xms256m</jvmFlag>
                            <jvmFlag>-Xmx256m</jvmFlag>
                        </jvmFlags>
                        <useCurrentTimestamp>true</useCurrentTimestamp>
                    </container>
                </configuration>
            </plugin>
````

+ 打包到本地
    jib:dockerBuild
+ 启动容器
```
docker run -d -p 8080:8080 --name [containers] [project-name]
```

### 相关文档
+ [官方配置说明](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin#quickstart)
