---
title: docker整理-启动
tags: docker
categories: web
mermaid: true
date: 2021-06-20
---   


## 说明 

## 主要命令
+ 重启 docker-compose -f your-compose.yml  up --force-recreate -d
+ 启动 docker-compose -f your-compose.yml up -d



## 举例
+ skywalking
```   
 version: '3.8'
 services:
   elasticsearch:
     image: docker.elastic.co/elasticsearch/elasticsearch-oss:7.4.2
     container_name: elasticsearch
     ports:
       - "9200:9200"
     healthcheck:
       test: [ "CMD-SHELL", "curl --silent --fail localhost:9200/_cluster/health || exit 1" ]
       interval: 30s
       timeout: 10s
       retries: 3
       start_period: 10s
     environment:
       - discovery.type=single-node
       - bootstrap.memory_lock=true
       - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
     ulimits:
       memlock:
         soft: -1
         hard: -1
 
   oap:
     image: apache/skywalking-oap-server:latest
     container_name: oap
     depends_on:
       elasticsearch:
         condition: service_healthy
     links:
       - elasticsearch
     ports:
       - "11800:11800"
       - "12800:12800"
     healthcheck:
       test: [ "CMD-SHELL", "/skywalking/bin/swctl ch" ]
       interval: 30s
       timeout: 10s
       retries: 3
       start_period: 10s
     environment:
       SW_STORAGE: elasticsearch
       SW_STORAGE_ES_CLUSTER_NODES: elasticsearch:9200
       SW_HEALTH_CHECKER: default
       SW_TELEMETRY: prometheus
       JAVA_OPTS: "-Xms2048m -Xmx2048m"
 
   ui:
     image: apache/skywalking-ui:latest
     container_name: ui
     depends_on:
       oap:
         condition: service_healthy
     links:
       - oap
     ports:
       - "8080:8080"
     environment:
       SW_OAP_ADDRESS: http://oap:12800
```

+ kafka
``` 
version: '3.5'
services:
  zookeeper:
    image: wurstmeister/zookeeper   ## 镜像
    container_name: zookeeper
    ports:
      - "2181:2181"                 ## 对外暴露的端口号
  kafka:
    image: wurstmeister/kafka       ## 镜像
    container_name: kafka
    volumes: 
        - /etc/localtime:/etc/localtime ## 挂载位置（kafka镜像和宿主机器之间时间保持一直）
    ports:
      - "9092:9092"
    environment:
      KAFKA_ADVERTISED_HOST_NAME: 127.0.0.1         ## 修改:宿主机IP
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181       ## 卡夫卡运行是基于zookeeper的
      KAFKA_ADVERTISED_PORT: 9092
      KAFKA_LOG_RETENTION_HOURS: 120
      KAFKA_MESSAGE_MAX_BYTES: 10000000
      KAFKA_REPLICA_FETCH_MAX_BYTES: 10000000
      KAFKA_GROUP_MAX_SESSION_TIMEOUT_MS: 60000
      KAFKA_NUM_PARTITIONS: 3
      KAFKA_DELETE_RETENTION_MS: 1000
  kafka-manager:
    image: sheepkiller/kafka-manager                ## 镜像：开源的web管理kafka集群的界面
    container_name: kafka-manager
    environment:
        ZK_HOSTS: 127.0.0.1                      ## 修改:宿主机IP
    ports:  
      - "9091:9000"                                 ## 暴露端口 9000这个端口冲突太多
```
  
 


## 参考
+ [菜鸟教程-docker](https://www.runoob.com/docker/docker-container-usage.html)
+ [docker官网](https://docs.docker.com/get-docker/)
+ [docker仓库](https://registry.hub.docker.com/search?q=&type=image) 
