---
title: jvm-不正规笔记
tags: [jvm]
categories: 工具
mermaid: true
date: 2022-06-12
---  



### jvm
+ 查看 JVM 默认参数
```  
-- HotSpot
java -XX:+PrintCommandLineFlags -version   
```

+ 查看 JVM 所有参数
```  
-- HotSpot
java -XX:+PrintFlagsFinal -version 
```

+ 查看某java进程的 JVM 配置
``` 
jinfo -flags [PID]
```

+ 对象加载情况
``` 
jmap -histo:live [PID] > demo.txt
```

+ 获取运行时内存快照
``` 
-- HotSpot
 

-- openj9
jcmd [PID] Dump.heap demo.dump
```

+ jstack
``` 
-- 查看具体的进程内部的线程情况
top -H -p [PID]

-- 转换为16进制的线程(上面比较消耗CPU的PID)
printf '%x\n' [PID]

-- 对比dump
```

+ 多进程执行脚本
```
#！/bin/bash

# 配置openj9以支持jmap脚本
# 获取java进程并记录对象使用情况
Time=`date +"%Y-%m-%d"`

for JPid in  `pgrep -f java` 
do
  echo $JPid
  JName=$(ps -ef | grep -v 'grep' | egrep $JPid | awk '{printf $13}')
  echo "$JName"
  /www/server/jdk8/bin/jmap -histo:live $JPid  > "$JName.txt"
done

```

### 参考配置
> -XX:PermSize设置非堆内存初始值，默认是物理内存的1/64
> -XX:MaxPermSize设置最大非堆内存的大小，默认是物理内存的1/4
> -Xms 最小堆内存，默认是物理内存的1/64
> -Xmx 最大堆内存，默认是物理内存的1/4 
> https://blog.csdn.net/lxacdf/article/details/123177694

### 参考
+ [语雀-jvm-尚硅谷](https://www.yuque.com/u21195183/jvm/qpoa81)
+ 
