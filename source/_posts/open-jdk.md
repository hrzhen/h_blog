---
title: jdk
tags: [运维]
categories: 工具
mermaid: true
date: 2022-06-01
---  

### linux安装

+ 下载/上传
```
wget -c  https://mirrors.tuna.tsinghua.edu.cn/Adoptium/11/jdk/x64/linux/OpenJDK11U-jdk_x64_linux_hotspot_11.0.16_8.tar.gz  
```
+ 解压
``` 
tar -xvf OpenJDK11U-jdk_x64_linux_openj9_linuxXL_11.0.10_9_openj9-0.24.0.tar.gz
```
+ 配置环境变量
```
-- 编辑环境变量
vim /etc/profile

-- 追加配置
export JAVA_HOME=/www/server/jdk11
export PATH=$JAVA_HOME/bin:$PATH

-- 生效文件
source /etc/profile
```
+ 测试
``` 
java -version
```



### 相关链接
+ [清华开源镜像库-openjdk-hostpot](https://mirrors.tuna.tsinghua.edu.cn/Adoptium/) 
+ [openj9文档](https://www.eclipse.org/openj9/docs/tool_jdmpview/#heapdump)
+ [阿里JVM-dragonwell11](https://github.com/alibaba/dragonwell11/releases)
