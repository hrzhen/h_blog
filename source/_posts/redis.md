---
title: redis
tags: redis
categories: 数据库
mermaid: true
date: 2021-06-20
---   

## 1 简介

### 1.1 是什么
    基于C++的键值对存储系统. 

### 1.2 特点
    存于内存.可持久化,单线程,支持主从同步,分布式

### 1.3 数据结构及常用方法

+ 字符串 String: 一个key一个val
    + get 获取值
    + set 设置值
    + incr 值加一
    + decr 值减一
    + mget 设置多个值
    + setex 以秒为有效期,并设值
+ 哈希表 Hash: 一个key多个filed,一个filed多个val
    + hget 根据key跟filed获取val
    + hset 根据key跟filed设置val
    + hgetall 根据key获取filed跟val
+ 列表 List: 有序的字符串列表
    + rpush  链表右侧插入
    + rpop 移除右侧列表头元素，并返回该元素 
    + lpop  移除左侧列表头元素，并返回该元素
    + lrange 获取list 区间内的所有元素
    + len 返回该列表的元素个数
+ Set:不重复的无序集合
    + sadd 存储多个val
    + spop 移除并返回集合中的一个随机元素
    + smembers 返回集合中的一个随机元素
    + sunion 返回多个key的并集，如果是单key就其全部
+ Sorted Set:有序的set
    + zadd  将一个或多个 member 元素及其 score 值加入到有序集 key 当中
    + zrange 返回有序集 key 中，指定区间内的 member
    + zrem 移除有序集 key 中的一个或多个 member
    + zcard 返回有序集的基数(值的总个数) 

## 2 场景

+ String: 常规key-value缓存应用,存储数字进行增减,有效期
+ Hash: 存储部分变更数据，如用户信息等
+ List: 消息队列系统 取最新N个数据的操作
+ Set: 交集，并集，差集, 不重复,无序
+ Sorted Set: 对比set增加了权重排序
+ stream: 消息队列
+ rejson: json支持

## 3 链接
+ [官方文档](https://redis.io/)
+ [参考中文文档](http://redisdoc.com/) 

