---
title: solon生态
tags: [国产化]
categories: web
mermaid: true
date: 2023-08-01
---  

### 生态
+ Solon
> 体验与 Spring Boot 相近


+ Water
> 功能相当于：consul + rabbitmq + elk + prometheus + openFaas + quartz + 等等，并有机结合在一起。 <br>
> 或者约等于：nacos + rocketmq + PlumeLog + prometheus + magic-api + xxl-job + 等。

+ luffy
> 嵌入式 FaaS 引擎 + 扩展中心 + 发布系统
> 其中 luffy-jtl 为h2版本 luffy-jtc 为mysql8
> 插件目前也是共用一个jvm,主要功能还是表结构,代码跟静态文件等的逻辑处理
> 通过 Java.type('java.lang.System') 获取原生的java类

| 组件                         | 说明                                          |
|-----------------------------|---------------------------------------------|
| org.noear:water.client      | 框架：Water 客户端                                |
| org.noear:water-solon-cloud-plugin | 框架：Water 客户端 for solon（也可用于 Spring Boot 项目） |
|                             |                                             |
| org.noear:waterapi          | 构建：Water 服务端                                |
| org.noear:watersev          | 构建：Water 后台服务（健康检测；数据监视；消息派发；定时任务等...）      |
| org.noear:wateradmin        | 构建：Water 控制台（支持LDAP登录）                      |
| org.noear:waterfaas         | 构建：Water FaaS 服务，提供轻量级FaaS服务                |
|                             |                                             |
| org.noear:xwater            | 构建：Water 助理工具                               |




### 参考链接
+ [官网](https://solon.noear.org/article/family-preview) 
+ [luffy](https://gitee.com/noear/luffy)