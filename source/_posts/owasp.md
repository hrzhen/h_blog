---
title: owasp
tags: [owasp]
categories: 安全
mermaid: true
date: 2023-06-15
---   

### 相关说明

+ 概述
> Open Web Application Security Project<br>
> 提供安全指南、工具、漏洞库以及开发安全的最佳实践等资源来改善Web应用程序的安全性.


+ HTML报告关键说明
1. Highest Severity  漏洞的最高严重度. low < Medium < High < Critical
2. CVE Count	 各个CVE漏洞的数量
3. Evidence Count 检测到的的证据数(漏洞检测来源/类型/国家地区)

+ 以java为例,一般依赖会更新相关漏洞,即更新依赖可解决漏洞问题.
 



###  maven插件

+ pom.xml
```xml
    <build>
        <plugins>
   			<plugin>
                <groupId>org.owasp</groupId>
                <artifactId>dependency-check-maven</artifactId>
                <version>8.2.1</version>
            </plugin>
        </plugins>
    </build>
```

+ 主要命令
```
check - 检查依赖中的已知漏洞 
purge - 清空缓存
update - 更新漏洞数据库
```


+ 查看 结果
``` 
target/dependency-check-report.html
```




## 备注



 

 