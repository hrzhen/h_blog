---
title: linux整理-1
tags: linux
categories: 服务器
mermaid: true
date: 2021-06-20
---   

## 常用脚本记录

###  vi/vim
```
-- 打开文件
vi [文件名]
-- 设置文档默认格式为unix 解决window/linux的文本问题
:set fileformat=unix
```

###  文本查看
```
-- 查看文件最后十来行 ,q退出
less  +G -n /home/construction-master/logs/sys-info.log

-- 在文件中查找模糊查询关键字
grep craftsman /home/construction-master/logs/sys-error.log | less
``` 


###  系统相关
```
-- 内存
free -h
总内存 已使用内存 完全空闲内存 多个进程共享内存 用于块设备数据缓冲 用于文件内容的缓冲 真正剩余的可被程序应用的内存数

-- 磁盘
 

-- 网络端口
netstat -ano|grep 808

-- 输出全格式的进程
ps -ef |grep java

查看进程执行所在目录
ll /proc/[PID]/exe


--性能分析工具 P cpu排序; M 内存排序
top 

根据进程ID查询线程情况
top -H -p [pid]



-- 文件查找, 根目录查询.jar后缀的所有文件详情
find / -name *.jar -ls

```

###  授权用户指定目录
``` 
chown -R dev:dev /www/wwwroot/cs.test.jzgjzj.com
chmod 777 /www/wwwroot/cs.test.jzgjzj.com
```

###  文件说明

|  路径  | 说明   |
| ---- | ---- |
| /etc/host | host文件,dns解析 |
|  |  |
| /etc/rc <br> /etc/rc.d  <br> /etc/rc*.d| 启动、或改变运行级时运行的scripts或scripts的目录. |
| /opt | 第三方软件 |
| /etc/passwd | 用户数据库，其中的域给出了用户名、真实姓名、家目录、加密的口令和用户的其他信息. |
| /etc/profile <br> /etc/csh.login <br> /etc/csh.cshrc| 登录或启动时Bourne或Cshells执行的文件.这允许系统管理员为所有用户建立全局缺省环境 |
| /sbin | 放置开机过程中所需要的用来设置系统环境的命令。里面包括了开机、修复、还原系统所需要的命令 |
