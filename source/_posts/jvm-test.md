---
title: jvm-测试记录
tags: [jvm]
categories: 工具
mermaid: true
date: 2022-06-13
---  


## jvm 测试记录 oracle-jdk8
+ 环境:  电脑剩下2G的时候运行服务 
+ 场景1 
```  
测试多次 100循环读取13.7M的文件
默认情况 heap可以超过3G
测试之后稳定下来 heap可以达到3G 
   PS Eden Space 1.14G
   PS Old Gen  1.96G
物理内存占用1.5G 
```
+ 场景2
```
测试多次 10/30 循环读取13.7M的文件

设置 -Xmx1G -Xms1G  
head不会超过1G 
测试稳定下来之后 
  PS Eden Space  260M
  PS Old Gen   700M
物理内存占用760M

因为超过就java head out
异常之后 程序依旧能够正常运行. 这里物理内存会超过1G,然后调用了一次GC,所以

  PS Eden Space  127M
  PS Old Gen   700M 其中使用的多余内存会被释放到未使用的空间
```
+ 小结
```
以上的类均会回收
但内存不会 抢占使用的内存(不管是head跟物理内存),大部分会被继续使用
从物理内存来说 以上测试不会减少实际物理内存的使用
```

## jdk11 
```
默认启用了G1 
同上面场景2的测试
一段时间后,物理内存占用会变低
```
