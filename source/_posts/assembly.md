---
title: assembly
tags: [maven,assembly]
categories: 工具
mermaid: true
date: 2021-06-26
---  

### 是什么
+ 定制化打包maven的插件
+ 目前记录的功能是拆包.springboot默认打包一个jar.
+ 需要打包拆分目录,方便增量部署,就可以使用此方案

### 怎么用
    
+ pom.xml
``` 
<packaging>jar</packaging>
<build>
		<resources> 
			<resource>
				<directory>src/main/java</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/*.class</include>
				</includes>
			</resource> 
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<!-- jar打包资源目录的包含格式 -->
					<include>*.xml</include>
					<include>*.txt</include>
				</includes>
			</resource>
		</resources>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.1</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<encoding>UTF-8</encoding>
					<compilerArgument>-parameters</compilerArgument>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.1.2</version>
				<configuration>
					<excludes>
						<exclude>*.*</exclude>
					</excludes>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>3.1.0</version>
				<executions>
					<execution>
						<id>make-assembly</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
						<configuration>
							<finalName>${project.artifactId}</finalName>
							<recompressZippedFiles>false</recompressZippedFiles>
							<appendAssemblyId>true</appendAssemblyId>
							<descriptors>
								<descriptor>${project.basedir}/bin/assembly.xml</descriptor><!-- 获取自定义的打包规则 -->
							</descriptors>
							<outputDirectory>${project.build.directory}/</outputDirectory><!-- 输出的最终打包目录 -->
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>

	</build>
```

+ assembly.xml
```
<?xml version="1.0" encoding="UTF-8"?>
<assembly>
    <!-- 文件编码/文件名 -->
    <id>${project.version}</id>

    <!-- 打包的类型，文件夹,linux/window的压缩包 -->
    <formats>
        <format>dir</format>
        <!-- <format>tar.gz</format>
         <format>zip</format>-->
    </formats>

    <fileSets>
        <!-- 项目根下面的脚本文件 copy 到根目录下 -->
        <fileSet>
            <directory>${project.basedir}/bin</directory>
            <outputDirectory></outputDirectory>
            <!-- 脚本文件在 linux 下的权限设为 755，无需 chmod 可直接运行 -->
            <fileMode>755</fileMode>
            <includes>
                <include>*.bat</include>
                <include>*.sh</include>
            </includes>
        </fileSet>

        <!-- 配置文件复制 到 /config 下 -->
        <fileSet>
            <directory>${basedir}/src/main/resources</directory>
            <outputDirectory>config</outputDirectory>
            <includes>
                <include>*.xml</include>
                <include>*.yml</include>
            </includes>
        </fileSet>

    </fileSets>
    <!-- 依赖的 jar 包 copy 到 lib 目录下 -->
    <dependencySets>
        <dependencySet>
            <outputDirectory>lib</outputDirectory>
        </dependencySet>
    </dependencySets>
</assembly>
```

+ PS: springboot得去掉默认的打包方式,即以下插件
```
spring-boot-maven-plugin
```


### 参考
+ [官方说明](http://maven.apache.org/plugins/maven-assembly-plugin/single-mojo.html)
+ [jfinal打包](https://jfinal.com/doc/1-3)
