---
title: linux整理-磁盘相关
tags: linux
categories: 服务器
mermaid: true
date: 2021-12-01
---   

## 功能 
+  当前目录磁盘占用情况
``` 
du -lh --max-depth=1
```

+  每秒刷新进度的读写情况
``` 
iotop -oP
```
 
+ 磁盘占用率 
```  
df -h
```
 


+ linux卸载本地磁盘报错解决
``` 
1、系统提示如下错误
umount: /tmp: target is busy
(In some cases useful info about processes thatuse the device is found by lsof(8) or fuser(1).)则执行下面命令，在umount
lsof /tmpltail -n +2 | awk 'print $21' sort -u xargsQ -l f kill 
2、系统提示如下错误
e2fsck 1.42.11 (09-Jul-2014)
/dev/mapper/root vg-lv tmp is mounted
e2fsck: Cannot continue, aborting.
则执行下面命令，
fuser -kuc /dev/mapper/root vg-lv tmp
```

``` 


1 卸载
 umount /dev/sda1

2 检查修复
fsck -C fd -N /dev/sda1

中间提示是否yes输入y即可，看到结束提示EILE SYSTEM WAS MODIEIED 重启系统后OK!
3 挂载
 mount /dev/sda1 /www/data
```
