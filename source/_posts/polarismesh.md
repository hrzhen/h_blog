---
title: polaris-mesh
tags: [运维]
categories: 微服务
mermaid: true
date: 2022-06-15
---  


### 项目说明
    腾讯开源的服务发现和治理中心
    解决分布式或者微服务架构中的服务可见、故障容错、流量控制和安全问题
    本身为golang开发,集成prometheus,支持多语言,grpc.支持集成其他微服务组件,如网关,以及nginx
    

| 组件 | 端口 | 概述 | 
| --- | --- |  --- | 
| prometheus | 	9090 | 	监控报警系统(时序数据库) | 
| pushgateway | 9091|  prometheus的推送网关 | 
| polaris console |	8080	| 可视化控制台，提供服务治理管控页面 | 
| polaris server |	8090(http)<br> 8091(grpc) |	控制面，提供数据面组件及控制台所需的后台接口 |


### 功能场景
+ 注册中心
> 基于 spring cloud 实现,没太大差别 .
> 这里增加了服务熔断等功能,未测试

+  配置中心
> 配置文件通过 groups 指定多个数组,以及多文件(支持目录) . 
> 文档没有特别标记 多个 groups的使用

+ Nginx插件
1. 安装编译
``` 

```
2. 测试
``` 

```


### 小结
> 对比其他微服务组件(阿里系/netflix...),相对完整且不怎么耗内存(golang出现内存问题目前更难排查) 
> 开源生态还在发展,短时间内很多功能例子不是特别丰富
> 

### 参考链接
+ [官网说明](https://polarismesh.cn/)
+ [github仓库](https://github.com/polarismesh) 
+ [polaris-mesh与spring-cloud版本对应](https://github.com/Tencent/spring-cloud-tencent/wiki/Spring-Cloud-Tencent-%E7%89%88%E6%9C%AC%E7%AE%A1%E7%90%86)
