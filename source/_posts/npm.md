**---
title: npm
tags: npm
categories: 前端
mermaid: true
date: 2022-06-20
---   



## 

+ 说明
```
前端的包管理工具
类同java中的maven
替代品还有yarn...
```
 
+ 远程仓库

|  路径  | 说明   |
| ---- | ---- |
|https://registry.npmjs.org/|npm默认|
| https://registry.npm.taobao.org/|淘宝|
|https://registry.nodejstsu.com/|nj|
|https://registry.yarnpkg.com/|yarn|


+ 相关命令

``` 
-- 全局配置
npm config list  

-- 全局插件
npm list -g

-- 全局插件版本信息
npm -g outdated

-- 当前项目升级依赖包
npm i -g --force

-- npm 升级

>> Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force
>> npm install -g npm-windows-upgrade
>> npm-windows-upgrade

```**




