---
title: nginx限流
tags: nginx
categories: nginx
mermaid: true
date: 2022-03-01
---


### 1 目的
> 限制单个ip最大访问流量以及并发数 

### 2 官方插件 
|插件| 说明|
|---|---|
|ngx_http_limit_conn_module | 连接数控制|
|ngx_http_limit_req_module|连接速率控制|

### 3 示例
```
    # 限制同一 IP 某段时间的访问量；
    # 以 $binary_remote_addr 为限制,存储到空间为 reqLimit 的缓存, 平均每秒不超过 10 个请求
    limit_req_zone $binary_remote_addr zone=reqLimit:10m rate=10r/s;
    server { 
        location / { 
            # burst 增加突发请求,多余的请求可以先放到队列
            # nodelay 降低排队时间,不增加上面的队列不会马上执行
            limit_req zone=reqLimit burst=4 nodelay;
            
            # 设置响应拒绝请求返回的状态码
            limit_req_status 501;
            
            # 反向代理后台服务
            proxy_pass http://127.0.0.1:8001;
        }
    }

    # 限制统一 IP 地址并发连接数
    # 以 $server_name 为限制,存储到空间为 serverLimit 的缓存
    limit_conn_zone $server_name zone=serverLimit:10m;
    server { 
        location / { 
            # 设置最大并发访问为100个
            limit_conn serverLimit 100;
            
            # 设置响应拒绝请求返回的状态码
            limit_conn_status 501;
            
            # 反向代理后台服务
            proxy_pass http://127.0.0.1:8001;
        }
    }

    limit_rate 100k; # 限制同一 IP 流量。 每秒限速 100KB

```

### 4 参考链接
+ [官方文档](http://nginx.org/en/docs/)
+ [非官方翻译参考](http://blog.yuqiyu.com/books/nginx)