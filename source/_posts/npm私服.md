---
title: npm私服
tags: npm
categories: 服务器
mermaid: true
date: 2021-10-31
---   

## nexus 
+ docker安装
``` 
docker run --restart=always -d -p 3300:8081 --name nexus sonatype/nexus3
```

+ 新增对应npm三种类型的仓库

+ 新增用户,赋予npm权限


## npm 

```
安装nrm管理
npm install -g nrm

查看已有源
npm ls

追加本地源
nrm add local_npm http://127.0.0.1/respository/local_npm/

切换原
nrm use local_npm

登录(用上上面做的账号)
npm login  --registry=http://127.0.0.1/respository/local_npm/

提交代码
npm publish
```


## 问题
+ nexus占用内存太大,当前版本(OSS 3.35.0-02)的启动方式在容器外也能看到java进程
+ docker mvn私服同理
