---
title: selenium
tags: [测试]
categories: 工具
mermaid: true
date: 2022-08-15
---  

### 啥玩意
+ 自动化测试工具
+ 有谷歌跟火狐插件 方便直接获取操作
+ 最通用的自动化工具

### java 举例   
``` 
          
           System.setProperty("webdriver.chrome.driver", "/home/demo/chromedriver.exe");  //本地浏览器 
           ChromeOptions options = new ChromeOptions();
           options.setHeadless(false); //非后台启动 
           options.setPageLoadStrategy(PageLoadStrategy.NORMAL);// 等待界面加载完成（html/JavaScript/图片，not ajax）           
           WebDriverManager.chromedriver().setup();//启动浏览器
           WebDriver driver = new ChromeDriver(options);    
            driver.get("https://www.baidu.com");// 打开页面

            // 元素操作
            driver.findElement(By.xpath("//*[@id=\"app\"]/div")).click();
            new Actions(driver).dragAndDropBy(driver.findElement(By.className("className")), 10, 0).perform();
            String readVal = driver.findElement(By.id("id")).getText();
            driver.findElement(By.xpath("//*[@id=\"app\"]/input")).sendKeys("演示的值")
            
    
```


 

### 说明
+ [谷歌浏览器驱动-chromedriver](https://registry.npmmirror.com/binary.html?path=chromedriver/)
+ [官网-selenium](https://www.selenium.dev/zh-cn/documentation/)
