---
title: linux整理-开机自启
tags: linux
categories: 服务器
mermaid: true
date: 2021-10-31
---   

## 开机自启 
+  创建自启脚本
``` 
vim /lib/systemd/system/prometheus.service

```

+ 编写
``` 
[Unit]
Description=Prometheus
After=network.target

[Service]
Type=simple
ExecStart=/www/server/prometheus/prometheus --config.file=/www/server/prometheus/prometheus.yml

[Install]
WantedBy=multi-user.target

```

+ 注册并执行脚本
```
systemctl daemon-reload
systemctl enable prometheus.service
systemctl restart prometheus.service
```

 