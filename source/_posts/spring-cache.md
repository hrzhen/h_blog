---
title: spring-cache
tags: [spring]
categories: spring
mermaid: true
date: 2022-05-19
---  

 

### 说明 
    读多写少,即时性,一致性要求不高的数据,通过增加缓存,减缓数据库的读操作
    

### 使用
+ 增加依赖 
``` 
    <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-cache</artifactId>
        </dependency>
// caffeine/redis...
```
+ 启动类开启注解
```
@EnableCaching
```
+ 配置注解参数
``` 
spring.cache.type=caffeine ## redis
```

+ 自定义缓存生成策略
```
org.springframework.cache.interceptor.KeyGenerator 
```
+ 自定义缓存管理
``` 
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        return new RedisCacheManager(
                RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory),
                this.getRedisCacheConfigurationWithTtl(5 * 60), // 默认策略，未配置的 key 会使用这个 5min
                this.getRedisCacheConfigurationMap() // 指定 key 策略
        );
    }
```

### 注解

| 注解        | 说明                            |
| ----------- | ------------------------------- |
| @Cacheable  | 触发缓存写入.缓存经常查询的数据      |
| @CacheEvict | 触发缓存删除.新增/删除/修改等操作      |
| @CachePut   | 更新缓存.方法正常运行 |
| @Caching   | 组合以上多个操作 |
| @CacheConfig   | 在类级别共享缓存的相同配置 |
  


### 可能的问题
+ 缓存穿透:查询一个null数据  spring.cache.redis.cache-null-values=true
+ 缓存击穿:大量并发进来同时查询一个正好过期的数据,因为默认无加锁,所以加锁即可 @Cacheable(sync = true)
+ 缓存雪崩:大量的key同时过期   缓存的时间随机区段,不固定
