---
title: exif
tags: [exif]
categories: 文件
mermaid: true
date: 2023-05-16
---   

### 概述

+ 文件说明

> 可交换图像文件格式（英语：Exchangeable image file format，官方简称Exif）<br>
> 包括 JPEG、TIFF、CR2、NEF、XMP


+ 相关工具
    + [java解析](https://github.com/drewnoakes/metadata-extractor)
    + [piexifjs](https://github.com/hMatoba/piexifjs)
    + [exif-js](https://github.com/exif-js/exif-js)
    + [exifr](https://github.com/MikeKovarik/exifr)

+ 完整例子(piexifjs)

```
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>拍照获取拍摄时间</title>
</head>
<body>
<video id="video" width="480" height="320" autoplay></video>

<br><br><br><br><br><br><br><br>
<button id="snap" style="width:500px;height:300px">拍照</button>
<p id="time"></p>


<script src="https://cdn.bootcdn.net/ajax/libs/piexifjs/1.0.6/piexif.min.js"></script>

<script>
const video = document.getElementById('video');
const snap = document.getElementById('snap');
const time = document.getElementById('time');

navigator.mediaDevices.getUserMedia({ video: true })
.then(stream => {
video.srcObject = stream;
video.play();
})
.catch(err => {
console.log(err);
});

snap.addEventListener('click', () => {
const canvas = document.createElement('canvas');
canvas.width = video.videoWidth;
canvas.height = video.videoHeight;
canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
const jpegData = canvas.toDataURL('image/jpeg') ;
let exifObj = piexif.load(jpegData);
console.log(exifObj)
for (var ifd in exifObj) {
console.log("-" + ifd);
for (var tag in exifObj[ifd]) {
console.log("  " + piexif.TAGS[ifd][tag]["name"] + ":" + exifObj[ifd][tag]);
}
}
});



</script>
</body>
</html>
```

+ 使用记录

```

基于可交换图像文件格式的类型.其拍摄的时候才有<br>
在经过微信等工具传输,或压缩转换等时,会去掉相关信息,进而不能解析<br>
通过canvas绘制的默认是png,哪怕指定为JPEG,也是没有相关信息的
```



 

 