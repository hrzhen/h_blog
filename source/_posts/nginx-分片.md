---
title: nginx-分片/slice
tags: nginx
categories: nginx
mermaid: true
date: 2023-09-01
---


### 1 特点
> 将原子请求分解为微件工作单元的能力,在需求允许的情况下大幅提高吞吐<br>
> 如分片下载,断点续传等等


 
### 2 配置
+  分片播放文件
``` 

# 定义缓存  
# 路径 
# levels  1:2 表示1位和2位16进制来命名目录 
# keys_zone 名称:大小
# inactive 缓存数据保留时间
# max_size 缓存最大大小限制,默认策略是LRU 
proxy_cache_path  /home/demo/nginx_cache/ levels=1:2 keys_zone=cache:100m inactive=600s max_size=5g;

server {
   location  ~* \.(mp4|avi|mov|mkv|flv)$  {
 
        slice             1m; # 每个最小单位 1M
        proxy_cache       cache; # 缓存名
        proxy_cache_key $uri$is_args$args$slice_range; # 缓存值 
        proxy_cache_valid 200 206 304 301 302 10d; # 设置缓存对象的有效期
        root /www/demo/oss_path; # 实际资源完整路径
    }
}

```
 