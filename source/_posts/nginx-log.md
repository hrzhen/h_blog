---
title: nginx-日志
tags: nginx
categories: nginx 
date: 2022-02-16
---


### 简介
> 日志记录用于问题排查跟行为采集等
> 默认的日志格式为本地文件持久化存储 
 
 
### 使用
+ 按日期yyy-mm-dd存储请求日志并设置日志格式
```
    http {
        log_format  main  '$remote_addr - $remote_user [$time_iso8601] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';
        server {
            if ($time_iso8601 ~ '(\d{4}-\d{2}-\d{2})') {
                    set $current_day $1;
            }
            access_log  logs/access-$current_day.log  main;
            listen 80; 
        }
    }

```
+ 设置日志格式为json字符串
``` 

    log_format main '{"timestamp": "$time_local", '
                        '"remote_addr": "$remote_addr", '
                        '"referer": "$http_referer", '
                        '"request": "$request", '
                        '"status": $status, '
                        '"bytes": $body_bytes_sent, '
                        '"agent": "$http_user_agent", '
                        '"x_forwarded": "$http_x_forwarded_for", '
                        '"up_addr": "$upstream_addr",'
                        '"up_host": "$upstream_http_host",'
                        '"up_resp_time": "$upstream_response_time",'
                        '"request_time": "$request_time"'
                        ' }';

    access_log  logs/access.log main;
```

 

 
### 日志参数

| 参数 | 说明                         | 示例                                                                                      |
| -------------------- |----------------------------|-----------------------------------------------------------------------------------------|                                                              
| $remote_addr             | 客户端地址                      | 211.28.65.253                                                                           | 
| $remote_user             | 客户端用户名称                    | --                                                                                      | 
| $time_local              | 访问时间和时区                    | 18/Jul/2012:17:00:01 +0800                                                              | 
| $request                 | 请求的URI和HTTP协议              | "GET /article-10000.html HTTP/1.1"                                                      | 
| $http_host               | 请求地址，即浏览器中你输入的地址（IP或域名）    | www.wang.com 192.168.100.100                                                            | 
| $status                  | HTTP请求状态                   | 200                                                                                     | 
| $upstream_status         | upstream状态                 | 200                                                                                     | 
| $body_bytes_sent         | 发送给客户端的内容大小                | 1547                                                                                    | 
| $body_bytes_sent         | 发送给客户端的数据大小(包括请求头)        | 1611                                                                                   | 
| $http_referer            | url跳转来源                    | https://www.baidu.com/                                                                  | 
| $http_user_agent         | 用户终端浏览器等信息                 | "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1; GTB7.0; .NET4.0C; | 
| $ssl_protocol            | SSL协议版本                    | TLSv1                                                                                   | 
| $ssl_cipher              | 交换数据中的算法                   | RC4-SHA                                                                                 | 
| $upstream_addr           | 后台upstream的地址，即真正提供服务的主机地址 | 10.10.10.100:80                                                                         | 
| $request_time            | 整个请求的总时间                   | 0.205                                                                                   | 
| $upstream_response_time  | 请求过程中，upstream响应时间         | 0.002                                                                                   | 

### 7 参考链接
+ [官方说明](http://nginx.org/en/docs/http/ngx_http_log_module.html) 