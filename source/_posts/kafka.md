---
title: kafka
tags: [消息队列,大数据]
categories: 消息队列
mermaid: true
date: 2022-09-20
---   

### 概述
+ 场景
1. 消息队列: 异步处理/应用解耦/流量削锋/日志处理等,即在耗时/复杂/高流量的场景下,进行中间过渡的一个中间件
2. 流处理:  支持进行事件流处理,存储的平台

+ 组件/api
1. Admin     API: 管理和检查主题、代理和其他Kafka对象
2. Producer  API: 向一个或多个Kafka主题发布(编写)事件流
3. Consumer  API: 订阅(阅读)一个或多个主题，并处理为其生成的事件流
4. Streams   API: 流处理应用,把事件流进行处理,多方式(转换/聚合/连接等)地将多输入流转换为多输出流
5. Connector API: 构建运行可重用的生产者/消费者,多种连接器(数据源,如MySQL)

+ 名词

|  名词  | 非主流的大白话   |
| ---- | ---- |
| 事件 event | 数据的存储结构,包括key,value,timestamp|
| 事件流 event streaming  |使得数据从n到m,包括中间进行处理的过程|


+ 特点
https://kafka.apache.org/documentation/#design
1. zookpeer 集群高可用
2. 大数据集成流程


### 参考链接
+[kafka官网](https://kafka.apache.org)
+[kafka生态](https://cwiki.apache.org/confluence/display/KAFKA/Ecosystem) 