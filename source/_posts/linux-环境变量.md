---
title: linux整理-环境变量
tags: linux
categories: 服务器
mermaid: true
date: 2022-05-20
---   

## 概要
    环境变量可以简单的分成用户自定义的环境变量以及系统级别的环境变量。
    用户级别环境变量定义文件：~/.bashrc、~/.profile（部分系统为：~/.bash_profile）
    系统级别环境变量定义文件：/etc/bashrc、/etc/profile(部分系统为：/etc/bash_profile）、/etc/environment
    另外在用户环境变量中，系统会首先读取~/.bash_profile（或者~/.profile）文件，如果没有该文件则读取~/.bash_login，根据这些文件中内容再去读取~/.bashrc
