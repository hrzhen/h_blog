---
title: quarkus
tags: [云原生]
categories: 云原生
mermaid: true
date: 2022-08-12
---  

### 概述
> 整合 vertx/jboos/eclipse/netty 等封装云原生服务.
> 简单web服务依赖比spring小.
> 支持拓展spring体系
> 但一些spring的功能上,不好找到解决示例.
> 对 vertx/netty 得有基础 上手就简单

### 常用注解

+ jax-rs:  Java Api for Restful Services

| 注解    | 说明 |
| :----------- | :----------- |
| @Path      | 资源标记,类/方法,值为uri的地址拼接(类+方法)<br>只有方法的注解,不能作为资源访问<br>空字符串就是/  |
| @GET   | get请求       |
| @POST   | post请求        |
| @PathParam   | URI中指定规则的参数 ,即 http://ip:port/classPath/methodPath/pathValue        |
| @QueryParam   | url拼接在?后面的参数        |
| @DefaultValue   | 设置默认值        |
| @FormParam   | POST请求且以form(MIME类型为 application/x-www-form-urlencoded )方式提交的表单的参数        |
| @FormDataParam   | POST请求且以form(MIME类型为 multipart/form-data )方式提交的表单的参数        |
| @BeanParam   | 类获取参数,里面通过@FormParam等方式获取        |
| @Produces   | HTTP响应的MIME类型，默认是*/*        |
| @Consumes   | HTTP请求的MIME类型，默认是*/*        |
| @NameBinding   | 绑定,配合实现aop等场景    |



+  JSR-330: 依赖注入
| 注解    | 说明 |
| :----------- | :----------- |
| @Singleton      | 使用该注解标记该类只创建一次，不能被继承。一般在类上用该注解       |
| @Named   | @Named和Spring的@Component功能相同。@Named可以有值，如果没有值生成的Bean名称默认和类名相同。        |
| @Inject    | 标识了可注入的构造器、方法或字段,即被上面注解过的<br>类同 Spring自带的@Autowired        |
| @Qualifier   | 和Spring的@Qualifier大致相同        |


### 接口
+ ContainerRequestFilter 请求拦截
+ ContainerResponseFilter 响应拦截

```  
@PreMatching 将在请求到资源之前执行 否则就后置执行
@NameBinding 绑定aop到具体的类或者方法 实现局部的方法拦截,没有即全局
```
