---
title: logback
tags: [日志]
categories: 日志
mermaid: true
date: 2024-01-08
---   

### slf4j
> 通过桥接模式实现,方便切换不同的日志实现  <br>
> Level  一般使用的日志级别 <br>
> Marker 标记   <br>
> MDC(Mapped Diagnostic Context) 为每个LoggingEvent添加属性值上下文
 

### logback
> org.apache.logging.logback.core.filter.Filter 过滤器默认接口 默认使用的是日志级别过滤 <br>
> ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy 按日志跟大小进行切割日志 跟文件写入联合使用 <br>
>

### 常用配置
``` 
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
 
    <property name="log_dir" value="/demo/log"/>

    <!-- 控制台输出 -->
    <appender name="console" class="ch.qos.logback.core.ConsoleAppender">
        <!-- 彩色日志 -->
        <layout class="ch.qos.logback.classic.PatternLayout">
            <pattern>
                %d{MM-dd HH:mm:ss} [%thread] %magenta(%-5level) %green([%logger{20}.%method-%line]) - %cyan(%msg) %n
            </pattern>
        </layout>
    </appender>

    <!-- loki 集成 -->
    <appender name="loki" class="com.github.loki4j.logback.Loki4jAppender">
        <batchMaxItems>100</batchMaxItems>
        <batchTimeoutMs>10000</batchTimeoutMs>
        <http>
            <url>http://127.0.0.1:3100/loki/api/v1/push</url>
        </http>
        <format class="com.github.loki4j.logback.JsonEncoder">
            <label>
                <pattern>type=test,service=v2-online,level=%level</pattern>
            </label>
            <message>
                <pattern>c=%logger{20} t=%thread | %msg %ex</pattern>
            </message>
            <sortByTime>true</sortByTime>
        </format>
    </appender>


    <!-- 本地文件输出 -->
    <appender name="api_file" class="ch.qos.logback.core.rolling.RollingFileAppender">

        <!-- 过滤器 -->
        <filter class="cn.xt.online.core.LogFilter">
            <level>INFO</level>
        </filter>

        <!-- 每小时写入一个文件 -->
        <file>${log_dir}/collect.log</file>
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <fileNamePattern>${log_dir}/collect_%d{yyyy-MM-dd-HH}.%i.log</fileNamePattern>
            <timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
                <maxFileSize>20MB</maxFileSize>
            </timeBasedFileNamingAndTriggeringPolicy>
            <maxHistory>24</maxHistory>
        </rollingPolicy>

        <!--日志格式-->
        <layout class="ch.qos.logback.classic.PatternLayout">
            <pattern>%msg%n</pattern>
        </layout>
        <encoder>
            <pattern>%msg%n</pattern>
        </encoder>
    </appender>


    <!-- 自定义的日志级别记录 优于根 -->
    <logger name="ch.qos.logback" level="warn"/>

    
    <!-- 根加载 以及默认记录级别  -->
    <root level="info">
        <appender-ref ref="api_file"/>
        <appender-ref ref="console"/>
    </root>

</configuration>

```

 