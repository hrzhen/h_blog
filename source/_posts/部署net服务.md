---
title: net服务迁移
tags: [ISS,MSSQL,C#]
categories: 服务器
mermaid: true
date: 2022-07-15
---  



### MSSQL
+ 通过docker安装 登录名为sa
``` 
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=SA123456" \
   -p 1433:1433 --name mssql --hostname mssql \
   -d \
   mcr.microsoft.com/mssql/server:2017-latest
```
+ 安装window的mssql客户端
+ 登录,并新建数据库,备份,复制原备份到容器目录,选择覆盖数据库的方式还原


### 部署项目
+ 启动window功能: IIS(补充万维网服务跟日志等)
+ iss增加网站,网站子路由的应用程序得先建立,挂载对物理路径即可


### 不正规问题记录
1. 需要安装iss跟万维网.以及net
2. 增加站点 注意 主机名为访问的域名对应nginx的serverName ip则是对应的本机IP,一般是* 
3. 访问503 需要调整应用程序的进程模型 applicationPoolIdentity -> NetworkService
4. 数据库连接配置参考
``` 
<database name="demodb" providerName="sqlserver" encrypted="false" debug="true" connectionString="Data Source=127.0.0.1;Initial Catalog=demodb;UID=sa;PWD=demodb666" />
<database name="demodb" providerName="sqlserver" encrypted="false" debug="true" connectionString="Data Source=iZacdyeodkynydZ\SQLEXPRESS;Initial Catalog=demodb;UID=sa;PWD=demodb666" />
```


### 参考 
+ [mssql docker安装](https://docs.microsoft.com/zh-cn/sql/linux/quickstart-install-connect-docker?view=sql-server-2017&pivots=cs1-cmd#connectexternal) 
+ [iss部署项目](https://www.cnblogs.com/qtiger/p/13859801.html)
+ [sql-server官方下载](https://www.microsoft.com/zh-cn/sql-server/sql-server-downloads)
+ [iis发布网站需要身份认证](https://blog.csdn.net/mengxiangjiutian/article/details/50202611)
+ [iss部署出现503](https://blog.csdn.net/veloi/article/details/83509203)
