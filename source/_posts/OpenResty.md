---
title: openresty
tags: nginx
categories: nginx
mermaid: true
date: 2023-09-01
---  

### 简要说明
> 基于nginx并用lua拓展.<br>
> 更方便实现请求中的二次开发<br>

### 生命周期
+ init_by_lua_file：master-initing 阶段，初始化全局配置或模块
+ init_worker_by_lua_file：worker-initing 阶段，初始化进程专用功能
+ ssl_certificate_by_lua_file：ssl 阶段，在握手时设置安全证书
+ set_by_lua_file：rewrite 阶段，改写 Nginx 变量
+ rewrite_by_lua_file：rewrite 阶段，改写 URI ，实现跳转或重定向
+ access_by_lua_file：access 阶段，访问控制或限速
+ content_by_lua_file：content 阶段，产生响应内容
+ balancer_by_lua_file：content 阶段，反向代理时选择后端服务器
+ header_filter_by_lua_file：filter 阶段，加工处理响应头
+ body_filter_by_lua_file：filter 阶段，加工处理响应体
+ log_by_lua_file：log 阶段，记录日志或其他的收尾工作

### 主要模块
+ ngx_lua: + ngx_lua基础模块,提供Lua与Nginx交互功能。
+ ngx.req/+ ngx.resp: 代表当前请求和响应对象。
+ ngx.location:用于发起子请求。
+ ngx.timer:定时器功能。
+ ngx.thread:线程管理。
+ ngx.shared.DICT:共享内存字典。
+ ngx.signal:信号处理。
+ ngx.socket:TCP/UDP客户端。
+ ngx.sockopt:socket选项。
+ ngx.cookie:操作Cookie。
+ ngx.header:请求/响应头管理。
+ ngx.ctx:请求上下文对象。
+ ngx.log:日志功能。
+ ngx.re 正则
+ redis/mysql....



 



### 参考链接

+ [openresty 核心说明](https://github.com/openresty/lua-nginx-module)