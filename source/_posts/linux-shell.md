---
title: linux整理-shell
tags: linux
categories: 服务器
mermaid: true
date: 2022-07-27
---   

## 脚本记录 
+  springboot部署: jar'md5更新
``` 
 path="/home/root/clouds" # 服务运行目录
 tar zxvf $path/clouds.tgz -C $path/cloud_tmp # 解压多个jar到临时目录
 chmod -R 777 $path/cloud_tmp

 ## 服务路径跟启动jar的字典
 declare -A smap 
 smap["clouds-a.jar"]="$path/clouds-a" 
 smap["clouds-b.jar"]="$path/clouds-b"
 smap["clouds-c.jar"]="$path/clouds-c" 
 
 for key in ${!smap[*]};do
     source_md5=`md5sum ${smap[$key]}/lib/$key | awk '{ print $1 }'`
     target_md5=`md5sum $path/cloud_tmp/$key | awk '{ print $1 }'`
     echo ${smap[$key]} $key $source_md5 $target_md5
     if [ ! ${source_md5} == ${target_md5} ]; then
         # 文件变化过就重启
         \cp -a $path/cloud_tmp/$key ${smap[$key]}/lib/
         cd ${smap[$key]}
         ./boot.sh restart
     fi
     #rm -f $path/cloud_tmp/$key
 done

```

+  springboot部署: 替换部分jar更新
``` 
 path="/home/root/clouds" # 服务运行目录
 tar zxvf $path/clouds.tgz -C $path/cloud_tmp # 解压多个jar到临时目录
 chmod -R 777 $path/cloud_tmp
  
 update_dir="$path/clouds_tmp"
 all_service_dir=("$path/clouds-a" "$path/clouds-b" "$path/clouds-c")

 for update_file in `ls $update_dir`; do # 遍历更新包 
     for service_dir in ${all_service_dir[*]}; do  # 遍历服务包
     if [ -e $service_dir/lib/$update_file ]; then  # 文件是否存在   
        \cp -a $update_dir/$update_file   $service_dir/lib/$update_file # 存在就替换
     fi
     done;
 done
 
 # 重启服务
 for service_dir in ${all_service_dir[*]}; do 
     $service_dir/boot.sh restart
 done; 
 

```
 
 
 
