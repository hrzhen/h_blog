---
title: playwright
tags: [测试]
categories: 工具
mermaid: true
date: 2023-05-05
---  

### 啥玩意
+ 自动化测试工具
+ 类同 selenium, 元素定位不局限xpath/css选择器
+ 增加了:丰富的页面监听(控制器/http请求响应),敏捷定位元素,自动登待...

### java 举例
0. 初始化一个窗口(可视化)
```
Page page =  Playwright.create().chromium().launch().newPage();
```
1. 模拟登录
```
            page.navigate("http://www.demo.xyz");
            page.fill("id=name", "root");
            page.fill(".pwd", "root"); 
            page.click("text=登录");
            page.waitForTimeout(10_000d); // 阻塞等待10s 
            page.evaluate("() => console.log(111) ");// 执行页面脚本
```
2. 页面监听: 请求,响应,控制台...
``` 
       page.onRequest(r->{
           r.headers().put("","")
       });
       page.onResponse(r->{
           r.body();
           r.statusText();
       });
       page.onConsoleMessage(c->{
           c.location();
           c.text();
           c.type();
       });
```
3. 上下文事件
``` 
    context.route("**", r -> {
        Map<String, String> headers =  r.request().headers();
        headers.put("","");// 设置请求头


        Route.ResumeOptions resumeOptions = new Route.ResumeOptions();
        String uri = r.request().url();
        if(...){
           String postData = r.request().postData();
           resumeOptions.setPostData(postData)
           r.resume(resumeOptions);// 修改参数继续请求
        }else if(...){
           r.abort();// 不请求
        }
    });
```


### 使用问题
1. 批量操作的时候,唤起的浏览器会偶尔卡顿(未测试后台执行的例子)
2. 监听事件的时候,数据报错大容易卡顿或者异常(一般不建议复杂操作),而且控制台日志没得清理功能,自己根据时间维护
3. 默认的自动等待去获取元素不太稳定,有可能是共性
4. 谷歌浏览器会被js脚本识别,火狐能绕过部分检测
5. [centos7等非Ubuntu LTS, 得自己安装依赖 有可能使用失败](https://playwright.dev/java/docs/next/cli/#install-system-dependencies)
```
yum install -y atk at-spi2-atk cups-libs libxkbcommon libXcomposite libXdamage libXrandr mesa-libgbm gtk3 libxshmfence alsa-lib-devel 
```

### 说明
+ [官网](https://playwright.dev/)
+ [java版源码](https://github.com/microsoft/playwright-java)