---
title: jpom
tags: [运维]
categories: 工具
mermaid: true
date: 2021-11-15
---  

###  简介
    简化版本的Jenkins
    通过一个server(web管理,编译打包)对应多个agent(实际部署服务器)
    本质: server把命令/文件通过ssh传输到agent端

### 名词说明
+ 节点: 实际运行的服务器,包含多个项目
+ 项目: web服务或者HTML静态目录,对应代码编译后的运行实例
+ 

### 简单上手
    举例使用,具体参考官网
+ 搭建环境 
1. 下载服务端初始化并启动
2. 登录地址设置初始化账号密码
3. 通过ssh新建节点并上传客户端关联


+ 构建项目并启动
1. 配置仓库信息
2. 新增构建(发布下面的脚本,就是传递给节点执行的)
3. 构建项目
 


### 参考
+ [jpom官网](https://jpom-site.keepbx.cn/docs/index.html#/) 
