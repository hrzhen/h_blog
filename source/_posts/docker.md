---
title: docker整理-启动
tags: docker
categories: web
mermaid: true
date: 2021-06-20
---   


## 说明
+ 相对虚拟机,减少不必要的资源输出,简单
+ 但是文件迁移/镜像升级等就不太方便,如 Nginx
+ mysql/web服务等 可选择把配置跟数据映射到宿主机

## 启动参数
|  参数 | 说明 |
| ---- | ---- | 
| cp | 复制文件,通过容器跟本地完整路径,格式: 源文件 目标文件 |
| -d  | 在后台运行容器并打印容器id |
| --restart |docker启动时启动容器,并重启策略 |
| -e | 配置环境变量,跟配置的dockerfile,以及其镜像联合使用 |
| --name [string]  | 为容器命名,方便后续操作代替容器id | 
|-p|将容器的端口发布到主机 格式为 宿主机端口:容器端口|



## 使用记录

```   

docker run -d -p host_port:container_port -v /host/path:/container/path myimage


docker run --restart=on-failure:10  -itd --name redis-test -p 6379:6379 redis

-- 拷贝进入容器
docker cp  /d:/soft/grafana/loki-config.yaml  loki:/etc/loki/

-- 拷贝容器的文件出来
docker cp loki:/etc/loki/loki-config.yaml e:/

-- loki
docker run --restart=on-failure:10 -d  --name=loki -p 3100:3100 grafana/loki

-- grafana
docker run --restart=on-failure:10 -d --name=grafana -p 3000:3000 grafana/grafana 
 

-- postgres 
docker run --restart=on-failure:10 -d --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=secret -e POSTGRES_USER=sentry postgres

-- mongo window10下 数据目录映射之后 重启有问题
docker run  --restart always  -p 27017:27017 --name mongodb  -e MONGO_INITDB_ROOT_USERNAME=111 -e MONGO_INITDB_ROOT_PASSWORD=111  -d mongo

# 导出docker镜像
docker save -o [容器编码/启动别名] > /home/demo.tar

# 导入docker镜像
docker load -i /home/demo.tar

# 查看容器日志
docker logs [容器编码/启动别名] 

# 进入容器
docker exec -it [容器编码] /bin/bash 
docker exec -it [容器编码] /bin/sh

# 停止容器
docker stop [容器编码]
# 删除容器
docker rm [容器编码]
# 删除镜像
docker rmi [镜像编码]



```

## 配置
    /etc/docker/deamon.json 新版为 /etc/docker/deamon.conf



## 参考
+ [菜鸟教程-docker](https://www.runoob.com/docker/docker-container-usage.html)
+ [docker官网](https://docs.docker.com/get-docker/)
+ [docker仓库](https://registry.hub.docker.com/search?q=&type=image) 
