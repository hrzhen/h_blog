---
title: nginx-auth
tags: nginx
categories: nginx
mermaid: true
date: 2022-03-11
---


### 1 目的
+ 通过后台自定义业务认证授权,再去访问静态资源或者反向代理等
+ 流程同spring中的拦截器

### 2 思路
+ 官方默认提供的认证插件如下
```
ngx_http_auth_basic_module      
ngx_http_auth_jwt_module
ngx_http_auth_request_module
``` 
+ request拦截满足上面基本需求


### 3 示例
``` 

server
{
    listen 80;
    server_name www.demo.auth.com;
 
    ##  业务访问的资源 
    location /oss { 

        ## 跨域
        add_header 'Access-Control-Allow-Origin' $http_origin;
        add_header 'Access-Control-Allow-Credentials' 'true';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT,web-token,app-token,Authorization,Accept,Origin,Keep-Alive,User-Agent,X-Mx-ReqToken,X-Data-Type,X-Auth-Token,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
        
        # 鉴权 只要返回内部status=401 就不会转发到下面
        auth_request /requestAuth;
        
        # 错误页面自定义
        error_page 401 = @error_auth;
        
        # 去掉访问中的前缀
        rewrite ^/oss/(.*)$ /$1 break;
        
        # 实际转发为路径
        root /www/server/minio-data/dev;
    }

   
    # 认证路由 由api服务校验业务
    location = /requestAuth {

        # 内部请求访问
        internal ; 

        # 追加定义请求头
        proxy_set_header X-Original-URI $request_uri;
        proxy_set_header Host $http_host;
        proxy_set_header Content-Length "";
       
        # 不发送http的body部分数据
        proxy_pass_request_body off; 
        

        # 实现拦截校验的接口 
        proxy_pass http://localhost:8888/opi/auth;    
    }

    # 认证失败修改自定义返回结果
    location @error_auth { 
       return 401 "{\"code\":-1,\"message\":\"request auth fail\",\"data\":null}";
    }



    ## 在线编码 
    location /idea {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        rewrite ^/idea/(.*)$ /$1 break;
        proxy_pass http://localhost:11800;
    }
 
}
```

### 4 参考链接
+ [基础认证](http://nginx.org/en/docs/http/ngx_http_auth_basic_module.html)
+ [请求认证](http://nginx.org/en/docs/http/ngx_http_auth_request_module.html)
+ [接口限流](http://nginx.org/en/docs/http/ngx_http_slice_module.html)